$(window).load(function() {
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('body').addClass('ios');
    }
    else {
        $('body').addClass('web');
        
    };
    setTimeout(function() {
        $('.canvas').addClass('showed_anim');
    }, 6000);
    $('body').removeClass('loaded');
    /* fancybox window */
    if($('.fancybox').length) {
        $('.fancybox').fancybox({
            margin: 10,
            padding: 0
        });
    };
    if($('.loop').length) {
        $('.loop').owlCarousel({
            center: true,
            items: 3,
            loop: true,
            nav: true,
            margin: 0,
            responsive: {
                480: {
                    items: 4
                },
                590: {
                    items: 5
                },
                768: {
                    items: 4
                },
                900: {
                    items: 5
                },
                1100: {
                    items: 6,
                    nav: false,
                },
                1300: {
                    items: 7
                },
                1500: {
                    items: 9
                },
                1800: {
                    items: 10
                }
            }
        });
    };
    /* fancybox window */
});
/* viewport width */
function viewport() {
    var e = window,
        a = 'inner';
    if(!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {
        width: e[a + 'Width'],
        height: e[a + 'Height']
    }
};
/* viewport width */
$(function() {
    if ($('.phone-mask').length) {
        $('.phone-mask').mask('+7 (999) 999 –  99 – 99');
    }


    if($('.js-popular').length) {
        $('.js-popular').slick({
            infinite: false,
            arrows: false,
            dots: true,
            speed: 300,
            unslick: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 50000,
                settings: "unslick"
            }, {
                breakpoint: 1100,
                settings: "slick"
            }, {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
    };
    if($('.option-list').length) {
        $('.option-list').slick({
            infinite: false,
            arrows: false,
            dots: true,
            speed: 300,
            unslick: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 50000,
                settings: "unslick"
            }, {
                breakpoint: 1200,
                settings: "slick"
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
    };

    if ($('.js-slider-input-1').length) {
        $(".js-slider-input-1").slider({
            range: 'min',
            min: 0, 
            max: 187000,
            value: 90000,
            slide: function( event, ui ) {
                $( ".js-slider-val-1" ).val( ui.value );
            }
        });
        $(".js-slider-val-1").val($( ".js-slider-input-1" ).slider( "value" ));
    }

    $(".js-slider-val-1").keyup(function(e){
        if (this.value > 0 && this.value < 187000) {
            this.value = parseInt(this.value)
            $(".js-slider-input-1").slider("value", parseInt(this.value));  
        } else if (this.value > 187000) {
            this.value = parseInt(187000)
            $(".js-slider-input-1").slider("value", 187000);
            
        } else {
            this.value = parseInt(0)
            $(".js-slider-input-1").slider("value", 0);  
        }
    });

    if ($('.js-slider-input-2').length) {
       $(".js-slider-input-2").slider({
           range: 'min',
           min: 0, 
           max: 12,
           value: 5,
           slide: function( event, ui ) {
               $( ".js-slider-val-2" ).val( ui.value );
           }
       });
       $(".js-slider-val-2").val($( ".js-slider-input-2" ).slider( "value" ));
   }

   $(".js-slider-val-2").keyup(function(e){
        if (this.value > 0 && this.value < 12) {
            this.value = parseInt(this.value)
            $(".js-slider-input-2").slider("value", parseInt(this.value));  
        } else if (this.value > 12) {
            this.value = parseInt(12)
            $(".js-slider-input-2").slider("value", 12);
        } else {
            this.value = parseInt(0)
            $(".js-slider-input-2").slider("value", 0); 
             
        }
    });


   // begin svg sircle
   if ($('.js-circle-1').length) {
       $('.js-circle-1').circliful({
           animationStep: 5,
           foregroundBorderWidth: 4,
           backgroundBorderWidth: 4,
           percent: 25,
       });
   }
   if ($('.js-circle-2').length) {
       $('.js-circle-2').circliful({
           animationStep: 5,
           foregroundBorderWidth: 4,
           backgroundBorderWidth: 4,
           percent: 50
       });
   }
   if ($('.js-circle-3').length) {
       $('.js-circle-3').circliful({
           animationStep: 5,
           foregroundBorderWidth: 4,
           backgroundBorderWidth: 4,
           percent: 75
       });
   }
   if ($('.js-circle-4').length) {
       $('.js-circle-4').circliful({
           backgroundColor: 'transparent',
           animationStep: 5,
           foregroundBorderWidth: 4,
           backgroundBorderWidth: 4,
           percent: 98
       });
    }

      

    
});
var handler = function() {

    var viewport_wid = viewport().width;

    if(viewport_wid <= 1200) {
        if($('.option-list').length) {
            $('.option-list').slick("getSlick").refresh();
        };
    };
    if(viewport_wid <= 1100) {
        if($('.js-popular').length) {
            $('.js-popular').slick("getSlick").refresh();
        };
    };
}
$(window).bind('load', handler);
$(window).bind('resize', handler);

